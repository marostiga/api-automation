Feature: sample karate test script

Background:
* url 'https://jsonplaceholder.typicode.com'

Scenario: get all users and then get the first user by id

Given path 'users'
And header cpf = '12345678901'
And header accountType = 'PF'
When method get
Then status 201
