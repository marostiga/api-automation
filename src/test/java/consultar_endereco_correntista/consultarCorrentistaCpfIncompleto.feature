Feature: Consultar Endereco Correntista Cpf Incompleto

Background:
* url '/api/v1/holders'

Scenario: Pesquisar por usuário existente com cpf incompleto

Given path 'address'
And header cpf = '1234567890'
And header accountType = 'PF'
When method get
Then status 400