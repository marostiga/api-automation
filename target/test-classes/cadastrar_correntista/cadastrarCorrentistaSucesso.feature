Feature: Happy Flow Cadastro de Correntista

Scenario: Cadastrar um novo correntista com informações válidas

* def user =
"""
{
		"holder": {
		"address": {
		"zipCode": "17000000",
		"street": "Street Test",
		"number": 10,
		"complement": "Complement Test"
		},
		"birthDate": "1990-09-25",
		"cpf": "12345678901",
		"accountType": "PF",
		"name": "User Test",
		"socialName": "Social Name Test"
		"cnpj": "12345678901234"
    }
}
"""

Given url '/api/v1/holders'
And request user
When method post
Then status 201

