Feature: Happy Flow Consultar Endereco Correntista

Background:
* url '/api/v1/holders'

Scenario: Pesquisar por usuário existente com dados válidos

Given path 'address'
And header cpf = '12345678901'
And header accountType = 'PF'
When method get
Then status 201