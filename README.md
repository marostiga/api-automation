# Automatização de API

Automatização de requisições de API utilizando Karate - Cucumber + Java (jUnit)

## Inicio

As instruções servirão para executar os testes em sua máquina local.

### Pré-requisitos

Java - JDK - JRE

* [Java-jdk](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html) - The official website
* [Java-jre](https://www.oracle.com/technetwork/pt/java/javase/downloads/jre8-downloads-2133155.html) - The official website

IDE - Eclipse

* [Eclipse](https://www.eclipse.org/downloads/) - The official website


## Execução dos testes

Após instalar os pré-requisitos, é necessário abrir o projeto na IDE (Eclipse).
Dentro do Eclipse, abra a pasta scr/test/java, abra a pasta 'cadastrar_correntistas', clique com o botão direto do mouse no arquivo 'CadastrarCorrentistaTest.java', vá até a opção 'Run As' e selecione 'jUnit Test'.
Nesse momento será executado o cenário de teste referente a essa classe. Os resultados aparecerão no lado esquerdo da IDE indicando onde ocorreu falha e sucesso durante o processo.
Também é apresentado um log cominformações do teste logo a baixo da IDE na aba'Console'.